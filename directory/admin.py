from django.contrib import admin
from .models import Warndevice


@admin.register(Warndevice)
class WarndeviceAdmin(admin.ModelAdmin):
    list_display = ['id', 'devtype', 'address', 'latitude', 'longitude', 'soundradius']

