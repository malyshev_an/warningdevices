from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models


class Warndevice(models.Model):
    DEVICE_TYPE = (
        ('Сирена', 'Сирена'),
        ('Громкоговоритель', 'Громкоговоритель')
    )

    devtype = models.CharField(max_length=17, choices=DEVICE_TYPE, verbose_name='Тип устройства')
    address = models.CharField(max_length=100, verbose_name='Адрес размещения')
    latitude = models.DecimalField(decimal_places=6, max_digits=8, verbose_name='Широта',
                                    validators=[MinValueValidator(-90), MaxValueValidator(90)])
    longitude = models.DecimalField(decimal_places=6, max_digits=9, verbose_name='Долгота',
                                    validators=[MinValueValidator(-180), MaxValueValidator(180)])
    soundradius = models.PositiveSmallIntegerField(verbose_name='Радиус зоны звукопокрытия')

    def __str__(self):
        return f'{self.devtype}, адрес размещения: {self.address}'

