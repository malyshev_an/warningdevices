from django import forms
from .models import Warndevice


class AddDeviceForm(forms.ModelForm):
    class Meta(object):
        model = Warndevice
        exclude = ('status',)
