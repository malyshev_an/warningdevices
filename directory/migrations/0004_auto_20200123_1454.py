# Generated by Django 3.0.2 on 2020-01-23 11:54

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('directory', '0003_auto_20200120_1322'),
    ]

    operations = [
        migrations.AlterField(
            model_name='warndevice',
            name='latitude',
            field=models.DecimalField(decimal_places=6, max_digits=8, validators=[django.core.validators.MinValueValidator(-90), django.core.validators.MaxValueValidator(90)], verbose_name='Широта'),
        ),
        migrations.AlterField(
            model_name='warndevice',
            name='longitude',
            field=models.DecimalField(decimal_places=6, max_digits=9, validators=[django.core.validators.MinValueValidator(-180), django.core.validators.MaxValueValidator(180)], verbose_name='Долгота'),
        ),
    ]
