from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from django.shortcuts import redirect
from django.views.generic import ListView, CreateView
from django.contrib import messages
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from .forms import AddDeviceForm
from .models import Warndevice
from .serializers import WarndeviceSerializer


class WarndeviceListView(ListView):
    model = Warndevice
    template_name = 'directory/show.html'
    context_object_name = 'devices'
    paginate_by = 4


class WarndeviceCreateView(CreateView):
    template_name = 'directory/add.html'
    form_class = AddDeviceForm

    def form_valid(self, form):
        form.save()
        messages.success(self.request, 'Новое устройство успешно добавлено!')
        return redirect('show')


class WarndeviceListAPIView(ListCreateAPIView):
    queryset = Warndevice.objects.all()
    serializer_class = WarndeviceSerializer
    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    filterset_fields = ['devtype']
    search_fields = ['address', 'latitude', 'longitude', 'soundradius']


class WarndeviceDetailAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Warndevice.objects.all()
    serializer_class = WarndeviceSerializer

