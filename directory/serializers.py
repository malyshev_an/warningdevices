from rest_framework import serializers
from .models import Warndevice


class WarndeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Warndevice
        fields = ['id', 'devtype', 'address', 'latitude', 'longitude', 'soundradius']
