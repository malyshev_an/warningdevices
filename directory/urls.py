from .views import WarndeviceListView, WarndeviceCreateView, WarndeviceListAPIView, WarndeviceDetailAPIView
from django.urls import path


urlpatterns = [
    path('', WarndeviceListView.as_view(), name="show"),
    path('page<int:page>/', WarndeviceListView.as_view()),
    path('add/', WarndeviceCreateView.as_view(), name="add"),
    path('api/', WarndeviceListAPIView.as_view()),
    path('api/<int:pk>/', WarndeviceDetailAPIView.as_view()),
]
